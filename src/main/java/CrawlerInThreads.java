import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 27.11.2017.
 */

public class CrawlerInThreads {

    private static final String PATH_TO_DIRECTORY = "C:\\Users\\User\\Desktop\\Project\\src\\main\\java\\examples";
    private static File directory = new File(PATH_TO_DIRECTORY);
    private static File[] files = directory.listFiles();
    private static Map<String, String> data = new ConcurrentHashMap<>();
    private static List<Thread> threadList = new ArrayList<>();
    private static int numOfThreads = 0;

    public static void main(String[] args) throws IOException {


        if (files == null || files.length == 0) {
            throw new IOException("Не обнаружены файлы по данной директории: " + PATH_TO_DIRECTORY);
        }
        for (int i = 0; i < files.length; i++) {
            String nameOfFile = files[i].getName();
            threadList.add(new Thread("Thread #" + i) {
                @Override
                public void run() {
                    System.out.println(getName() + " was activated");
                    numOfThreads++;
                    readTheFile(PATH_TO_DIRECTORY + "\\" + nameOfFile, nameOfFile);
                }
            });
        }

        System.out.println("Start");
        for (Thread currentThread : threadList) {
            currentThread.start();
            //currentThread.join();
            //Thread.sleep(500);
        }
    }

    private static synchronized void setTheData(String key, String value) {
        data.put(key, value);
    }

    private static void readTheFile(String filePath, String nameOfFile) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null) {

                Pattern pattern = Pattern.compile("(extends|implements) [a-zA-Z]+");
                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    setTheData(nameOfFile, matcher.group());
                    //System.out.println(nameOfFile + ": " + matcher.group());
                }
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("Проблема с файлом " + nameOfFile);
        } finally {
            if (numOfThreads == threadList.size()) {
                System.out.println("Output map of data: " + data);
            }
        }
    }
}